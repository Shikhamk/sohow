
// sidebar
function toggleSidebar() {
  var sidebar = document.querySelector('.sidebar');
  if (sidebar.style.display === 'none' || sidebar.style.display === '') {
    sidebar.style.display = 'block';
  } else {
    sidebar.style.display = 'none';
  }
}



// dropdown section
var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
  return new bootstrap.Popover(popoverTriggerEl)
})



  // pagination
  function showPage(pageNumber) {
    // Hide all pages
    document.getElementById('contentPage1').style.display = 'none';
    document.getElementById('contentPage2').style.display = 'none';
    // Show the selected page
    document.getElementById(`contentPage${pageNumber}`).style.display = 'block';
    // Update active state in the pagination
    const paginationItems = document.querySelectorAll('#pagination-nb li');
    paginationItems.forEach(item => item.classList.remove('active'));
    paginationItems[pageNumber - 1].classList.add('active');
  }


  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})
